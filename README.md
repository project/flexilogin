# Flexi Login

**Flexi Login** allows users to use more flexible identifying
credential to be recognized during login.  The identifier can be
*either* their Drupal username, the email associated with that
username, or a phone number associated with that username.

As far as possible, phone number formatting is ignored. This means
that the user is not required to remember the exact formatting used
when he or she added the phone number to the account settings.

As always, the correct password or passphrase associated with the
account imust also be given to successfully log in.


## Requirements

* [**Advanced Help Hint**][01]:  
  To hint about getting `README.md` displayed as help.
* [**Phone Field**][02]:  
  Provides a field type to hold phone numbers.


## Recommended modules

* [**Advanced Help Hint**][01]:  
  To hint about getting `README.md` displayed as help.
* [**Advanced Help**][03]:  
  When this module is enabled, the project's `README.md` will be
  shown on the screen when you visit `help/flexilogin/README.md`.
* [**Markdown**][04]:  
  When this module is enabled, the project's `README.md` will look
  better when rendered on the screen.


## Installation

Install as you would normally install a contributed drupal
module. See: [Installing modules][05] for further information.


## Configuration

Enabling the module changes login processing to allow the user to
identify using *either* a username, an email address, or a phone
number (if one exists). Disabling the module reverts to Drupal's
default behavior (only the username is a valid identifier).

The default Drupal user entity already has fields for a username and
user email, but is missing one for a phone number.

To allow users to log in using a phone number, a field of type
“Phonefield” (available when you enable the **Phone Field** module)
must be part of the user's profile.  If such a field does not already
exist you must create one: Navigate to *Configuration » People »
Account settings* and click on the *Manage fields* tab.  Add a new
field of the type “Phonefield”. Press <em>Save</em> to save the new
field.

You also need to navigate to navigate to *Configuration » People »
Flexi Login settings* to set or confirm the profile field to use for
the phone number.

You need the persission *administer users* to be able to do this.

To make sure a phone number field is treated as an identifying
credential, navigate to *Configuration » People » Flexi Login
settings* and check that a field is selected.  Click “Save
configuration” or “Confirm” to confirm the field.

## Limitations

The error message seen by the user when login fails is the standard
Drupal error message and only mentions *username* (not email address
or phone number).  I would like to change this message into one
that lists all three means of user identification, but there is no API
call for this. (It has been requested: [Allow forms to set custom
validation error messages on required fields][06].)  However, if you
really want to alter this error message, you may use [**String
Overrides**][07].

## Troubleshooting

* You entered “12345678”. If this was meant to be a phone number,
  please note that **Flexi Login** on this website has not yet been
  configured to look up phone numbers.
  See under “Configuration” above to learn how to set or confirm the
  profile field to use for the phone number.


## Bug reports, support requests, patches

Post bug reports, support requests, patches and reports about
related/incompatible modules to the [project's issue queue][08].

The aim of this project is to have a simple and lightweight module
with almost no administrative interface that allows users more
flexibility when logging in.  This means that new features will *not*
be added unless there exists a really good use case for the request.

Help with development from the community (e.g. patches, reviews, bug
reports) are welcome.

## Related modules

The following modules offer a similar service and may be used instead
of this:

* [**Email Registration**][09]: Allow users to register and login using
  only an email address.
* [**LoginToboggan**][10]: Allow users to login using either their
  username or their email address and other features listed on its
  project page.

These modules are similar to **Flexi Login** as they allow users to
login with an email address instead of, or in addition to their
username.  None of them allow the use of a phone number as
identifier.

Please note this project is incompatible with these related modules
(and probably all modules that alters the login form in some way).
You need to disable all other login-modules before you install
**Flexi Login**.

If you know about other modules that should be listed as related
and/or incompatible, please let me know.

## Maintainers

The project is maintained by [Hannemyr Nye Medier AS][11].  
The company can be contacted for paid customizations of this project
as well as general Drupal services (including installation,
development, customizations and hosting).

[01]: https://www.drupal.org/project/advanced_help_hint
[02]: https://www.drupal.org/project/phonefield
[03]: https://www.drupal.org/project/advanced_help
[04]: https://www.drupal.org/project/markdown
[05]: https://www.drupal.org/docs/7/extend/installing-modules
[06]: https://www.drupal.org/node/742344
[07]: https://www.drupal.org/project/stringoverrides
[08]: https://www.drupal.org/project/issues/flexilogin
[09]: https://www.drupal.org/project/email_registration
[10]: https://www.drupal.org/project/logintoboggan
[11]: https://www.drupal.org/hannemyr-nye-medier-as
