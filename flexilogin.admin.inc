<?php

/**
 * @file
 * Administrative configuration for the Flexi Login module.
 */

/**
 * Menu callback: Provide settings page for Flexi Login.
 *
 * @return array
 *   Form.
 */
function flexilogin_default_field() {
  $instances = field_info_instances('user');
  $options = array();
  foreach ($instances['user'] as $name => $field) {
    if ('phonefield_field' == $field['widget']['type']) {
      $options[$name] = $field['label'];
    }
  }
  if (empty($options)) {
    drupal_set_message(t("To let users use their telephone number to identify, the user's account settings must have a field of type “Phonefield”. Please add this field."), 'warning');
    drupal_goto('/admin/config/people/accounts/fields');
  }
  $phonefield = variable_get('flexilogin_phonefield', NULL);
  $msg = ($phonefield) ? t("Phone field to use has machine name “<code>@field</code>”.", array('@field' => $phonefield)) : t("Phone field to use has not been set yet.");
  $form['flexilogin_status'] = array(
    '#markup' => $msg,
  );
  $card = count($options);
  if (1 == $card) {
    variable_set('flexilogin_phonefield', key($options));
    $form['flexilogin_h3'] = array(
      '#markup' => t('<h3>Name telephone number field</h3>'),
    );
    $form['flexilogin_phonefield'] = array(
      '#markup' => t("The existing profile field that shall be used for looking up the telephone number as a credential is “@field”.", array('@field' => reset($options))),
    );
    $form['actions']['cancel'] = array(
      '#markup' => '<p>' . l(t('Confirm'), '/', array('attributes' => array('class' => array('form-cancel-button')))) . '</p>',
    );
  }
  else {
    $form['flexilogin_phonefield'] = array(
      '#type' => 'select',
      '#title' => t('Select the profile field to use when a telephone number shall be used as a credential.'),
      '#default_value' => variable_get('flexilogin_phonefield', ''),
      '#options' => $options,
      '#description' => t("This field should be part of the user's profile."),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save configuration',
    );
  }
  return $form;
}

/**
 * Menu callback: Provide page to provide custom strings for Flexi Login .
 *
 * @return array
 *   Form.
 */
function flexilogin_override_strings() {
  $site_name = variable_get('site_name', '');
  $form['flexilogin_strings'] = array(
    '#markup' => t('<h3>Override default strings</h3><p>Overide the string shown to the user in the login block.<br />In the text fields below, the variable “<code>@site_name</code>” will be replaced by “<em>@xsite</em>”.</p>', array('@xsite' => $site_name))
  );
  $form['flexilogin_identifier_tip'] = array(
    '#type' => 'textfield', 
    '#title' => t('Override the tip for the identifier field.'),
    '#description' => t('The identifier tip tells the user what to use as an identifier when logging in.'),
    '#default_value' => variable_get('flexilogin_identifier_tip', FLEXILOGIN_IDENTIFIER_TIP), 
    '#size' => 90,
    '#maxlength' => 160, 
  );
  $form['flexilogin_password_tip'] = array(
    '#type' => 'textfield', 
    '#title' => t('Override the tip for the password field.'),
    '#description' => t('The password tip tells the user what to enter in the password field when logging in.'),
    '#default_value' => variable_get('flexilogin_password_tip', FLEXILOGIN_PASSWORD_TIP), 
    '#size' => 90,
    '#maxlength' => 160, 
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save strings',
    '#submit' => array('flexilogin_save_strings'),
  );
  $form['reset'] = array(
    '#type' => 'submit',
    '#value' => 'Reset strings to default',
    '#submit' => array('flexilogin_default_strings'),
  );
  return $form;
}

/**
 * Submit for the flexilogin_config form.
 */
function flexilogin_default_field_submit($form, &$form_state) {
  variable_set('flexilogin_phonefield', $form_state['values']['flexilogin_phonefield']);
  drupal_set_message(t('Configuration saved.'));
}

/**
 * Submit for the flexilogin_config save strings form.
 */
function flexilogin_save_strings($form, &$form_state) {
  variable_set('flexilogin_identifier_tip', $form_state['values']['flexilogin_identifier_tip']);
  variable_set('flexilogin_password_tip', $form_state['values']['flexilogin_password_tip']);
  drupal_set_message(t('Strings saved.'));
}

/**
 * Submit for the flexilogin_config default strings form.
 */
function flexilogin_default_strings($form, &$form_state) {
  variable_set('flexilogin_identifier_tip', FLEXILOGIN_IDENTIFIER_TIP);
  variable_set('flexilogin_password_tip', FLEXILOGIN_PASSWORD_TIP);
  drupal_set_message(t('Strings reset to default values.'));
}
